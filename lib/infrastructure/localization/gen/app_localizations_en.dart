import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get appTitle => 'get_skeleton';

  @override
  String get home => 'home';

  @override
  String get settings => 'settings';

  @override
  String get code => 'code';

  @override
  String get quote_empty => 'Quotes are empty';

  @override
  String get email => 'email';

  @override
  String get password => 'password';

  @override
  String get login => 'login';

  @override
  String get logout => 'logout';

  @override
  String get passwordConfirmationNotEquals => 'Password Confirmation Not Equals';

  @override
  String get verification => 'Verification';

  @override
  String get enterOtpCode => 'Enter the code send to email';

  @override
  String get didntReceiveCode => 'Did\'nt Receive Code?';

  @override
  String get resend => 'Resend';
}
