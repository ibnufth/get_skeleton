import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:get_skeleton/infrastructure/references/key.dart';

class Routes {
  static Future<String> get initialRoute async {
    final result = await Get.find<FlutterSecureStorage>().read(key: kToken);
    if (result == null) {
      return LOGIN;
    }
    return HOME;
  }

  static const HOME = '/home';
  static const LOGIN = '/login';
  static const PROFILE = '/profile';
  static const OTP = '/otp';
  static const REGISTER = '/register';
}
