export 'package:get_skeleton/infrastructure/navigation/bindings/controllers/home.controller.binding.dart';
export 'package:get_skeleton/infrastructure/navigation/bindings/controllers/login.controller.binding.dart';
export 'package:get_skeleton/infrastructure/navigation/bindings/controllers/quote.controller.binding.dart';
export 'package:get_skeleton/infrastructure/navigation/bindings/controllers/otp.controller.binding.dart';
export 'package:get_skeleton/infrastructure/navigation/bindings/controllers/register.controller.binding.dart';
