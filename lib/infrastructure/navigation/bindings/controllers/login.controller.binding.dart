import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/domain/use_case/login.dart';
import 'package:get_skeleton/features/auth/presentation/controllers/login.controller.dart';

class LoginControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginUseCase(Get.find()));
    Get.lazyPut<LoginController>(
      () => LoginController(Get.find()),
    );
  }
}
