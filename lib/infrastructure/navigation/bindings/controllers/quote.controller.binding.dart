import 'package:get/get.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_local_data_source.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_remote_data_source.dart';
import 'package:get_skeleton/features/quote/data/repositories/quote_repository_impl.dart';
import 'package:get_skeleton/features/quote/domain/repositories/quote_repository.dart';
import 'package:get_skeleton/features/quote/domain/use_case/get_random_quote.dart';
import 'package:get_skeleton/features/quote/presentation/controllers/quote.controller.dart';

class QuoteControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<QuoteLocalDataSource>(
        () => QuoteLocalDataSourceImpl(isar: Get.find()));
    Get.lazyPut<QuoteRemoteDataSource>(
        () => QuoteRemoteDataSourceImpl(api: Get.find()));
    Get.lazyPut<QuoteRepository>(() => QuoteRepositoryImpl(
          quoteRemoteDataSource: Get.find(),
          quoteLocalDataSource: Get.find(),
          internetConnection: Get.find(),
        ));
    Get.lazyPut(() => GetRandomQuotes(Get.find()));
    Get.lazyPut<QuoteController>(() => QuoteController()..getRandomQuotes());
  }
}
