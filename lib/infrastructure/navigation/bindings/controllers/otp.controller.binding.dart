import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/domain/use_case/otp_verification.dart';
import 'package:get_skeleton/features/auth/domain/use_case/request_otp.dart';
import 'package:get_skeleton/features/auth/presentation/controllers/otp.controller.dart';

class OTPControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => OTPVerificationUseCase(Get.find()));
    Get.lazyPut(() => RequestOTPUseCase(Get.find()));
    Get.lazyPut<OTPController>(
      () => OTPController(
        otpVerificationUseCase: Get.find(),
        requestOTPUseCase: Get.find(),
      ),
    );
  }
}
