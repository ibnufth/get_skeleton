import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/domain/use_case/register.dart';
import 'package:get_skeleton/features/auth/presentation/controllers/register.controller.dart';

class RegisterControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RegisterUseCase(Get.find()));
    Get.lazyPut<RegisterController>(
      () => RegisterController(Get.find()),
    );
  }
}
