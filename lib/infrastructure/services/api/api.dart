import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart' hide Response;
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../../config.dart';

class Api {
  static Api? _instance;

  final APIClient api;

  factory Api({required APIClient apiClient}) =>
      _instance ?? Api._(api: apiClient);

  Api._({required this.api});

  Future<Response<T>> get<T>(
    String url, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await api.dio
        .get<T>(url, queryParameters: queryParameters, data: data);
  }

  Future<Response<T>> post<T>(String url,
      {dynamic data,
      Map<String, dynamic>? queryParameters,
      Options? options}) async {
    return await api.dio.post(url,
        data: data, queryParameters: queryParameters, options: options);
  }

  void useAccessToken([String? token]) {
    
    if (token != null && token.isNotEmpty) {
      api.dio.options.headers[HttpHeaders.authorizationHeader] =
          "Bearer $token";
      api.dio.options.headers[HttpHeaders.contentTypeHeader] =
          "application/json";
    } else {
      api.dio.options.headers.remove(HttpHeaders.authorizationHeader);
    }
  }
}

class APIClient {
  final FlutterSecureStorage _secureStorage = Get.find();
  Dio? _dio;

  Dio get dio {
    if (_dio != null) {
      return _dio!;
    } else {
      _dio = Dio(
        BaseOptions(
          headers: {
            "Accept": "application/json",
          },
          baseUrl: ConfigEnvironments.getEnvironments()['url']!,
          receiveTimeout: const Duration(seconds: 30),
          connectTimeout: const Duration(seconds: 150),
          sendTimeout: const Duration(seconds: 30),
        ),
      );
      _dio?.interceptors.add(APIInterceptors(storage: _secureStorage));
      _dio?.interceptors.add(PrettyDioLogger());
      return _dio!;
    }
  }

  APIClient._internal();

  static final _singleton = APIClient._internal();

  factory APIClient() => _singleton;
}

class APIInterceptors extends Interceptor {
  final FlutterSecureStorage storage;

  APIInterceptors({required this.storage});

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    var accessToken = await storage.read(key: "token");

    if (accessToken != null) {
      options.headers[HttpHeaders.authorizationHeader] = "Bearer $accessToken";
    }

    return handler.next(options);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    switch (err.type) {
      case DioExceptionType.connectionTimeout:
        throw ServerException(
            message:
                'Terjadi Kesalahan, Periksa konesi internet Anda, Mohon Coba Lagi');
      case DioExceptionType.sendTimeout:
        throw ServerException(
            message:
                'Terjadi Kesalahan, Periksa konesi internet Anda, Mohon Coba Lagi');
      case DioExceptionType.receiveTimeout:
        throw ServerException(
            message:
                'Terjadi Kesalahan, Periksa konesi internet Anda, Mohon Coba Lagi');
      case DioExceptionType.connectionError:
        throw DeadlineExceededException(
            err.requestOptions, err.response?.data["data"]['message']);
      case DioExceptionType.badResponse:
        switch (err.response?.statusCode) {
          case 400:
            throw BadRequestException(
                err.requestOptions, err.response?.data["data"]['message']);
          case 401:
            {
              throw UnauthorizedException(
                  err.requestOptions, err.response?.data["data"]['message']);
            }
          case 403:
            throw ForbiddenException(
                err.requestOptions, err.response?.data["data"]['message']);
          case 404:
            throw NotFoundException(
                err.requestOptions, "Terjadi Kesalahan, Mohon Coba Lagi nanti");
          case 409:
            throw ConflictException(
                err.requestOptions, err.response?.data["data"]['message']);
          case 422:
            throw InvalidInputException(
                err.requestOptions, err.response?.data["data"]['message']);
          case 500:
            throw InternalServerErrorException(
                err.requestOptions, err.response?.data["data"]['message']);
          default:
            throw DioServerException(
                err.requestOptions, 'Terjadi Kesalahan, Mohon Coba Lagi nanti');
        }

      case DioExceptionType.cancel:
        throw ServerException(message: 'Cancel');
      case DioExceptionType.badCertificate:
        throw ServerException(message: 'Bad Certificate');
      case DioExceptionType.unknown:
        throw ServerException(
            message: err.response?.data['message'] ?? 'Unknown');
    }
  }
}
