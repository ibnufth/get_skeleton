// ignore_for_file: public_member_api_docs, sort_constructors_first
// coverage:ignore-file
import 'package:equatable/equatable.dart';

class Failure extends Equatable {
  final String? message;
  const Failure({
    this.message,
  });

  @override
  List<Object?> get props => [message];

  @override
  bool get stringify => true;
}

class CachedFailure extends Failure {
  const CachedFailure({super.message});

  @override
  String toString() => "CachedFailure: $message";
}

class ServerFailure extends Failure {
  const ServerFailure({super.message});

  @override
  String toString() => "ServerFailure: $message";
}
