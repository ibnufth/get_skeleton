export 'package:get_skeleton/features/home/home.screen.dart';
export 'package:get_skeleton/features/auth/presentation/login.screen.dart';
export 'package:get_skeleton/features/user/presentation/profile.screen.dart';
export 'package:get_skeleton/features/auth/presentation/otp.screen.dart';
export 'package:get_skeleton/features/auth/presentation/register.screen.dart';
