// ignore_for_file: public_member_api_docs, sort_constructors_first
class User {
  final String? uid;
  final String? username;
  final String? email;
  final String? avatar;
  User({
    this.uid,
    this.username,
    this.email,
    this.avatar,
  });

  User copyWith({
    String? uid,
    String? username,
    String? email,
    String? avatar,
  }) {
    return User(
      uid: uid ?? this.uid,
      username: username ?? this.username,
      email: email ?? this.email,
      avatar: avatar ?? this.avatar,
    );
  }

  @override
  String toString() {
    return 'User(uid: $uid, username: $username, email: $email, avatar: $avatar)';
  }

  @override
  bool operator ==(covariant User other) {
    if (identical(this, other)) return true;

    return other.uid == uid &&
        other.username == username &&
        other.email == email &&
        other.avatar == avatar;
  }

  @override
  int get hashCode {
    return uid.hashCode ^ username.hashCode ^ email.hashCode ^ avatar.hashCode;
  }

  

  
}
