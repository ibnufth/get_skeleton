import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';

abstract class UserRepository {
  Future<Either<Failure, User>> getActiveUser();
  Future<Either<Failure, bool>> editActiveUser({required User user});
  Future<Either<Failure, bool>> deleteActiveUser();
}
