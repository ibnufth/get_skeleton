import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/features/user/domain/repositories/user_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class GetActiveUser implements UseCase<User, NoParams> {
  final UserRepository userRepository;

  GetActiveUser(this.userRepository);

  @override
  Future<Either<Failure, User>> call(NoParams params) {
    return userRepository.getActiveUser();
  }
}
