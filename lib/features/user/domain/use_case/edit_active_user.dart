import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/features/user/domain/repositories/user_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class EditActiveUser implements UseCase<bool, User> {
  final UserRepository userRepository;

  EditActiveUser(this.userRepository);

  @override
  Future<Either<Failure, bool>> call(User params) {
    return userRepository.editActiveUser(user: params);
  }
}
