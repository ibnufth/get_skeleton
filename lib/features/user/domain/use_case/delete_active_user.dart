import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/user/domain/repositories/user_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class DeleteActiveUser implements UseCase<bool, NoParams> {
  final UserRepository userRepository;

  DeleteActiveUser(this.userRepository);

  @override
  Future<Either<Failure, bool>> call(NoParams params) {
    return userRepository.deleteActiveUser();
  }
}
