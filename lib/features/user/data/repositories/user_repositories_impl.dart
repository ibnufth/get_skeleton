// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/user/data/data_sources/user_local_data_source.dart';
import 'package:get_skeleton/features/user/data/data_sources/user_remote_data_source.dart';
import 'package:get_skeleton/features/user/data/models/user_model.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/features/user/domain/repositories/user_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';

class UserRepositoryImpl implements UserRepository {
  final UserRemoteDataSource userRemoteDataSource;
  final UserLocalDataSource userLocalDataSource;
  UserRepositoryImpl({
    required this.userRemoteDataSource,
    required this.userLocalDataSource,
  });

  @override
  Future<Either<Failure, bool>> deleteActiveUser() async {
    try {
      final result = await userLocalDataSource.deleteActiveUser();
      return right(result);
    } on CacheException catch (ex) {
      return left(CachedFailure(message: ex.message));
    }
  }

  @override
  Future<Either<Failure, bool>> editActiveUser({required User user}) async {
    try {
      final remoteResult = await userRemoteDataSource.editActiveUser(
          userModel: UserModel.fromEntity(user));
      if (remoteResult) {
        final result = await userLocalDataSource.editActiveUser(
            userModel: UserModel.fromEntity(user));
        return right(result);
      }
      return right(remoteResult);
    } on CacheException catch (ex) {
      return left(CachedFailure(message: ex.message));
    } on ServerException catch (ex) {
      return left(ServerFailure(message: ex.message));
    }
  }

  @override
  Future<Either<Failure, User>> getActiveUser() async {
    try {
      try {
        final result = await userLocalDataSource.getActiveUser();
        return right(result);
      } on CacheException {
        final result = await userRemoteDataSource.getActiveUser();
        return right(result);
      }
    } on ServerException catch (ex) {
      return left(CachedFailure(message: ex.message));
    }
  }
}
