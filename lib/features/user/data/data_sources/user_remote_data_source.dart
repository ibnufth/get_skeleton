import 'package:get_skeleton/features/user/data/models/user_model.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/services/api/api.dart';

abstract class UserRemoteDataSource {
  Future<UserModel> getActiveUser();
  Future<bool> editActiveUser({required UserModel userModel});
}

class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final Api api;

  UserRemoteDataSourceImpl(this.api);

  @override
  Future<bool> editActiveUser({required UserModel userModel}) {
    // TODO: implement editActiveUser
    throw ServerException(message: "UnimplementedError()");
  }

  @override
  Future<UserModel> getActiveUser() {
    // TODO: implement getActiveUser
    throw ServerException(message: "UnimplementedError()");
  }
}
