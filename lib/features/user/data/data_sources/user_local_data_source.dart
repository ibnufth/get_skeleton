import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_skeleton/features/user/data/models/user_model.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/references/key.dart';

abstract class UserLocalDataSource {
  Future<UserModel> getActiveUser();
  Future<bool> editActiveUser({required UserModel userModel});
  Future<bool> addActiveUser({required UserModel userModel});
  Future<bool> deleteActiveUser();
}

class UserLocalDataSourceImpl implements UserLocalDataSource {
  final FlutterSecureStorage flutterSecureStorage;

  UserLocalDataSourceImpl(this.flutterSecureStorage);

  @override
  Future<bool> addActiveUser({required UserModel userModel}) async {
    try {
      await flutterSecureStorage.write(key: kUser, value: userModel.toJson());
      return true;
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }

  @override
  Future<bool> deleteActiveUser() async {
    try {
      await flutterSecureStorage.delete(key: kUser);
      return true;
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }

  @override
  Future<bool> editActiveUser({required UserModel userModel}) async {
    try {
      await flutterSecureStorage.write(key: kUser, value: userModel.toJson());
      return true;
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }

  @override
  Future<UserModel> getActiveUser() async {
    try {
      final result = await flutterSecureStorage.read(key: kUser);
      if (result == null) {
        throw CacheException(message: "Empty data");
      }
      return UserModel.fromJson(result);
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }
}
