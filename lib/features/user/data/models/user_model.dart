import 'dart:convert';

import 'package:get_skeleton/features/user/domain/entities/user.dart';

class UserModel extends User {
  UserModel({
    super.uid,
    super.username,
    super.email,
    super.avatar,
  });

  factory UserModel.fromEntity(User entity) => UserModel(
        uid: entity.uid,
        username: entity.username,
        email: entity.email,
        avatar: entity.avatar,
      );

  @override
  UserModel copyWith({
    String? uid,
    String? username,
    String? email,
    String? avatar,
  }) {
    return UserModel(
      uid: uid ?? this.uid,
      username: username ?? this.username,
      email: email ?? this.email,
      avatar: avatar ?? this.avatar,
    );
  }

  @override
  String toString() {
    return 'UserModel(uid: $uid, username: $username, email: $email, avatar: $avatar)';
  }

  @override
  bool operator ==(covariant UserModel other) {
    if (identical(this, other)) return true;

    return other.uid == uid &&
        other.username == username &&
        other.email == email &&
        other.avatar == avatar;
  }

  @override
  int get hashCode {
    return uid.hashCode ^ username.hashCode ^ email.hashCode ^ avatar.hashCode;
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'uid': uid,
      'username': username,
      'email': email,
      'avatar': avatar,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      uid: map['uid'] != null ? map['uid'] as String : null,
      username: map['username'] != null ? map['username'] as String : null,
      email: map['email'] != null ? map['email'] as String : null,
      avatar: map['avatar'] != null ? map['avatar'] as String : null,
    );
  }
  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
