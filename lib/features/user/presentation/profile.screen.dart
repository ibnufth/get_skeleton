import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_skeleton/features/user/presentation/controller/user.controller.dart';
import 'package:get_skeleton/infrastructure/localization/gen/app_localizations.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UserController>(
      initState: (controller) {},
      builder: (userController) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Profile"),
          ),
          body: userController.isLoading.value
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                
                  children: [
                    ClipRRect(
                      child: Image.network(
                          "${userController.activeUsers.value?.avatar}"),
                    ),
                    Text("${userController.activeUsers.value?.username}"),
                    Text("${userController.activeUsers.value?.email}"),
                    Text("${userController.activeUsers.value?.uid}"),
                    const SizedBox(
                      height: 64,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        userController.logout();
                      },
                      child: Text("${AppLocalizations.of(context)?.logout}"),
                    ),
                  ],
                ),
        );
      },
    );
  }
}
