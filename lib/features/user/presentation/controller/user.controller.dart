import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/domain/use_case/logout.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/features/user/domain/use_case/get_active_user.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';
import 'package:get_skeleton/infrastructure/navigation/routes.dart';

class UserController extends GetxController {
  final GetActiveUser getActiveUser;
  final LogoutUseCase logoutUseCase;
  RxBool isLoading = false.obs;
  Rxn<User> activeUsers = Rxn();
  UserController(this.getActiveUser, this.logoutUseCase);

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  void setActiveUser(User user) {
    activeUsers.value = user;
    update();
  }

  void initData() async {
    if (activeUsers.value == null) {
      isLoading.value = true;
      update();
      final result = await getActiveUser(NoParams());
      result.fold((l) {
        Get.offAllNamed(Routes.LOGIN);
      }, (r) {
        activeUsers.value = r;
      });
      isLoading.value = false;
      update();
    }
  }

  void logout() async {
    isLoading.value = true;
    update();
    final result = await logoutUseCase(NoParams());
    result.fold((l) {}, (r) {
      Get.offAllNamed(Routes.LOGIN);
    });
    isLoading.value = false;
    update();
  }
}
