import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get_skeleton/infrastructure/localization/gen/app_localizations.dart';

class FormPassword extends StatefulWidget {
  final String name;
  final String? label;
  final String? hintText;
  final bool isConfirmation;
  final String? parentPassword;
  final AutovalidateMode? autovalidateMode;
  final void Function(String? value)? onChanged;
  final String? Function(String?)? validators;
  const FormPassword({
    Key? key,
    required this.name,
    this.label,
    this.hintText,
    this.isConfirmation = false,
    this.parentPassword,
    this.onChanged,
    this.autovalidateMode,
    this.validators,
  }) : super(key: key);

  @override
  State<FormPassword> createState() => _FormPasswordState();
}

class _FormPasswordState extends State<FormPassword> {
  bool isShow = false;

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: widget.name,
      obscureText: !isShow,
      onChanged: widget.onChanged,
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(),
        if (widget.isConfirmation)
          FormBuilderValidators.equal(widget.parentPassword ?? "",
              errorText:
                  "${AppLocalizations.of(context)?.passwordConfirmationNotEquals}"),
        if (widget.validators != null) widget.validators!
      ]),
      autovalidateMode: widget.autovalidateMode,
      decoration: InputDecoration(
          errorMaxLines: 2,
          label: Text(widget.label ?? widget.name),
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                isShow = !isShow;
              });
            },
            icon: Icon(
              isShow ? Icons.visibility : Icons.visibility_off,
            ),
          ),
          hintText: widget.hintText,
          floatingLabelBehavior: FloatingLabelBehavior.never),
    );
  }
}
