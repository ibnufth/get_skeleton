import 'package:flutter/material.dart';
import 'package:get/get.dart';

void errorSnackbar(String title, String message) {
  Get.snackbar(
    title,
    message,
    backgroundColor: Colors.red,
    colorText: Colors.white,
  );
}

void successSnackbar(String title, String message) {
  Get.snackbar(
    title,
    message,
    backgroundColor: Colors.green,
    colorText: Colors.white,
  );
}

void warningSnackbar(String title, String message) {
  Get.snackbar(
    title,
    message,
    backgroundColor: Colors.yellow,
    colorText: Colors.black45,
  );
}

void infoSnackbar(String title, String message) {
  Get.snackbar(
    title,
    message,
    backgroundColor: Colors.blue,
    colorText: Colors.white,
  );
}
