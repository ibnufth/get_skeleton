import 'package:get_skeleton/features/quote/data/models/quote_model.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/references/api_references.dart';
import 'package:get_skeleton/infrastructure/services/api/api.dart';

abstract class QuoteRemoteDataSource {
  Future<List<QuoteModel>> getRandomQuotes();
}

class QuoteRemoteDataSourceImpl extends QuoteRemoteDataSource {
  final Api api;

  QuoteRemoteDataSourceImpl({required this.api});

  @override
  Future<List<QuoteModel>> getRandomQuotes() async {
    try {
      final response = await api.get(APIReference.getRandom);
      var quotes = (response.data["result"] as List)
          .map((e) => QuoteModel.fromMap(e))
          .toList();
      return quotes;
    } catch (e) {
      throw ServerException(message: e.toString());
    }
  }
}
