import 'package:get_skeleton/features/quote/data/models/quote_model.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:isar/isar.dart';

abstract class QuoteLocalDataSource {
  Future<List<QuoteModel>> getRandomQuotes();

  Future<bool> saveQuotes({required List<QuoteModel> quotes});
}

class QuoteLocalDataSourceImpl extends QuoteLocalDataSource {
  final Isar isar;

  QuoteLocalDataSourceImpl({required this.isar});

  @override
  Future<List<QuoteModel>> getRandomQuotes() async {
    try {
      return await isar.quoteModels.where().findAll();
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }

  @override
  Future<bool> saveQuotes({required List<QuoteModel> quotes}) async {
    try {
      return await isar.writeTxn(() async {
        final result = await isar.quoteModels.putAll(quotes);
        return result.isNotEmpty;
      });
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }
}
