import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_local_data_source.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_remote_data_source.dart';
import 'package:get_skeleton/features/quote/data/models/quote_model.dart';
import 'package:get_skeleton/features/quote/domain/entites/quote.dart';
import 'package:get_skeleton/features/quote/domain/repositories/quote_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';

class QuoteRepositoryImpl implements QuoteRepository {
  final QuoteRemoteDataSource quoteRemoteDataSource;
  final QuoteLocalDataSource quoteLocalDataSource;
  final InternetConnection internetConnection;

  QuoteRepositoryImpl({
    required this.quoteRemoteDataSource,
    required this.quoteLocalDataSource,
    required this.internetConnection,
  });

  @override
  Future<Either<Failure, List<Quote>>> getRandomQuotes() async {
    List<QuoteModel> quotes = [];
    if (await internetConnection.hasInternetAccess) {
      try {
        quotes = await quoteRemoteDataSource.getRandomQuotes();
        await quoteLocalDataSource.saveQuotes(quotes: quotes);
      } on ServerException {
        try {
          quotes = await quoteLocalDataSource.getRandomQuotes();
        } on CacheException catch (e) {
          return left(CachedFailure(message: e.message));
        }
      } on CacheException catch (e) {
        return left(CachedFailure(message: e.message));
      }
    } else {
      try {
        quotes = await quoteLocalDataSource.getRandomQuotes();
      } on CacheException catch (e) {
        return left(CachedFailure(message: e.message));
      }
    }
    return right(quotes);
  }
}
