// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:isar/isar.dart';

import 'package:get_skeleton/features/quote/domain/entites/quote.dart';

part 'quote_model.g.dart';

@collection
class QuoteModel extends Quote {
  @override
  final Id? id;
  @override
  final String? english;
  @override
  final String? indo;
  @override
  final String? character;
  @override
  final String? anime;

  const QuoteModel({
    this.id,
    this.english,
    this.indo,
    this.character,
    this.anime,
  });

  factory QuoteModel.fromEntity(Quote entity) => QuoteModel(
        id: entity.id,
        english: entity.english,
        indo: entity.indo,
        character: entity.character,
        anime: entity.anime,
      );

  @override
  QuoteModel copyWith({
    Id? id,
    String? english,
    String? indo,
    String? character,
    String? anime,
  }) {
    return QuoteModel(
      id: id ?? this.id,
      english: english ?? this.english,
      indo: indo ?? this.indo,
      character: character ?? this.character,
      anime: anime ?? this.anime,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'english': english,
      'indo': indo,
      'character': character,
      'anime': anime,
    };
  }

  factory QuoteModel.fromMap(Map<String, dynamic> map) {
    return QuoteModel(
      id: map['id'] != null ? map['id'] as int : null,
      english: map['english'] != null ? map['english'] as String : null,
      indo: map['indo'] != null ? map['indo'] as String : null,
      character: map['character'] != null ? map['character'] as String : null,
      anime: map['anime'] != null ? map['anime'] as String : null,
    );
  }

  @override
  String toString() {
    return 'QuoteModel(id: $id, english: $english, indo: $indo, character: $character, anime: $anime)';
  }

  @override
  bool operator ==(covariant QuoteModel other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.english == english &&
        other.indo == indo &&
        other.character == character &&
        other.anime == anime;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        english.hashCode ^
        indo.hashCode ^
        character.hashCode ^
        anime.hashCode;
  }
}
