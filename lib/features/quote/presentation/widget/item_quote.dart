import 'package:flutter/material.dart';
import 'package:get_skeleton/features/quote/domain/entites/quote.dart';
import 'package:get_skeleton/infrastructure/localization/gen/app_localizations.dart';

class ItemQuote extends StatelessWidget {
  const ItemQuote({
    super.key,
    required this.quote,
  });

  final Quote quote;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Card(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 24),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${AppLocalizations.of(context)?.appTitle.toUpperCase()}",
              style: theme.textTheme.titleLarge?.copyWith(
                color: theme.colorScheme.secondary,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 8),
            Text(
              "${quote.english}",
              style: theme.textTheme.bodyLarge
                  ?.copyWith(fontStyle: FontStyle.italic),
            ),
            const SizedBox(height: 8),
            Text(
              "${quote.indo}",
              style: theme.textTheme.bodyLarge
                  ?.copyWith(decoration: TextDecoration.underline),
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "${quote.character}, ${quote.anime}",
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: theme.colorScheme.tertiary,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      )),
    );
  }
}
