import 'package:get/get.dart';
import 'package:get_skeleton/features/quote/domain/entites/quote.dart';
import 'package:get_skeleton/features/quote/domain/use_case/get_random_quote.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class QuoteController extends GetxController {
  final GetRandomQuotes _getRandomQuotes;

  RxBool isLoading = false.obs;
  RxnString errorMessage = RxnString();
  RxList<Quote> quotes = RxList.empty();

  QuoteController({GetRandomQuotes? getRandomQuotes})
      : _getRandomQuotes = getRandomQuotes ?? Get.find();

  getRandomQuotes() async {
    isLoading.value = true;
    update();
    final result = await _getRandomQuotes(NoParams());

    result.fold((l) {
      errorMessage.value = l.message;
    }, (r) {
      errorMessage.value = null;
      quotes.clear();
      quotes.addAll(r);
    });
    isLoading.value = false;
    update();
  }
}
