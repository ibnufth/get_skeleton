import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_skeleton/features/quote/presentation/controllers/quote.controller.dart';
import 'package:get_skeleton/infrastructure/localization/gen/app_localizations.dart';

import 'widget/item_quote.dart';

class QuoteScreen extends GetView<QuoteController> {
  const QuoteScreen({super.key});

  @override
  Widget build(BuildContext context) {
    // var theme = Theme.of(context);
    return GetBuilder<QuoteController>(
      builder: (controller) {
        if (controller.isLoading.value) {
          return const Center(child: CircularProgressIndicator());
        } else if (controller.errorMessage.value != null) {
          return Center(child: Text(controller.errorMessage.value!));
        } else if (controller.quotes.isEmpty) {
          return Center(
              child: Text("${AppLocalizations.of(context)?.quote_empty}"));
        }
        return RefreshIndicator(
          onRefresh: () async {
            controller.getRandomQuotes();
          },
          child: ListView.builder(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            itemCount: controller.quotes.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return ItemQuote(quote: controller.quotes[index]);
            },
          ),
        );
      },
    );
  }
}
