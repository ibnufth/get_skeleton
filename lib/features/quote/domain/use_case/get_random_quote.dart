import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/quote/domain/entites/quote.dart';
import 'package:get_skeleton/features/quote/domain/repositories/quote_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class GetRandomQuotes implements UseCase<List<Quote>, NoParams> {
  final QuoteRepository quoteRepository;

  GetRandomQuotes(this.quoteRepository);

  @override
  Future<Either<Failure, List<Quote>>> call(NoParams params) {
    return quoteRepository.getRandomQuotes();
  }
}
