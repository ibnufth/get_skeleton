// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:isar/isar.dart';

class Quote {
  final Id? id;
  final String? english;
  final String? indo;
  final String? character;
  final String? anime;

  const Quote({
    this.id,
    this.english,
    this.indo,
    this.character,
    this.anime,
  });

  Quote copyWith({
    int? id,
    String? english,
    String? indo,
    String? character,
    String? anime,
  }) {
    return Quote(
      id: id ?? this.id,
      english: english ?? this.english,
      indo: indo ?? this.indo,
      character: character ?? this.character,
      anime: anime ?? this.anime,
    );
  }

  @override
  bool operator ==(covariant Quote other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.english == english &&
        other.indo == indo &&
        other.character == character &&
        other.anime == anime;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        english.hashCode ^
        indo.hashCode ^
        character.hashCode ^
        anime.hashCode;
  }

  @override
  String toString() {
    return 'Quote(id: $id, english: $english, indo: $indo, character: $character, anime: $anime)';
  }
}
