import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/quote/domain/entites/quote.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';

abstract class QuoteRepository {
  Future<Either<Failure, List<Quote>>> getRandomQuotes();
}
