import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_skeleton/features/quote/presentation/quote.screen.dart';
import 'package:get_skeleton/infrastructure/navigation/routes.dart';

import 'controllers/home.controller.dart';

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomeScreen'),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {
                Get.toNamed(Routes.PROFILE);
              },
              icon: const Icon(Icons.person)),
        ],
      ),
      body: const QuoteScreen(),
    );
  }
}
