import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_skeleton/features/user/data/models/user_model.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/references/key.dart';
import 'package:get_skeleton/infrastructure/services/api/api.dart';

abstract class AuthRemoteDataSource {
  Future<bool> login({required String email, required String password});

  Future<bool> logout();

  Future<UserModel> otpVerification({required String otp});

  Future<bool> requestOtp();

  Future<bool> register({required UserModel user, required String password});
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final Api api;
  final FlutterSecureStorage flutterSecureStorage;

  AuthRemoteDataSourceImpl(this.flutterSecureStorage, this.api);

  @override
  Future<bool> login({required String email, required String password}) async {
    // TODO: implement login
    api.useAccessToken("tokenCobaCoba");
    await flutterSecureStorage.write(key: kToken, value: 'tokenCobaCoba');
    return true;
  }

  @override
  Future<bool> logout() async {
    api.useAccessToken();
    return true;
  }

  @override
  Future<UserModel> otpVerification({required String otp}) async {
    // TODO: implement otp verification
    return UserModel(
        uid: "Abkad12312qsdba",
        email: "email@email.com",
        username: "user1",
        avatar: "https://picsum.photos/200");
  }

  @override
  Future<bool> requestOtp() async {
    // TODO : implement request otp
    throw ServerException(message: "UnImplemented Error");
  }

  @override
  Future<bool> register(
      {required UserModel user, required String password}) async {
    // TODO: implement register
    return true;
  }
}
