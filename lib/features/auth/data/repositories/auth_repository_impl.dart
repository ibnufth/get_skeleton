import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/auth/data/data_sources/auth_remote_data_source.dart';
import 'package:get_skeleton/features/auth/domain/repositories/auth_repository.dart';
import 'package:get_skeleton/features/user/data/data_sources/user_local_data_source.dart';
import 'package:get_skeleton/features/user/data/models/user_model.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';

class AuthRepositoryImpl implements AuthRepository {
  final UserLocalDataSource userLocalDataSource;
  final AuthRemoteDataSource authRemoteDataSource;

  AuthRepositoryImpl(
      {required this.userLocalDataSource, required this.authRemoteDataSource});

  @override
  Future<Either<Failure, bool>> login(
      {required String email, required String password}) async {
    try {
      final result =
          await authRemoteDataSource.login(email: email, password: password);
      return right(result);
    } on ServerException catch (ex) {
      return left(ServerFailure(message: ex.message));
    } on CacheException catch (ex) {
      return left(CachedFailure(message: ex.message));
    }
  }

  @override
  Future<Either<Failure, bool>> logout() async {
    try {
      await authRemoteDataSource.logout();
      await userLocalDataSource.deleteActiveUser();
      return right(true);
    } on ServerException catch (ex) {
      return left(ServerFailure(message: ex.message));
    } on CacheException catch (ex) {
      return left(CachedFailure(message: ex.message));
    }
  }

  @override
  Future<Either<Failure, User>> otpVerification(String otp) async {
    try {
      final result = await authRemoteDataSource.otpVerification(otp: otp);
      await userLocalDataSource.addActiveUser(userModel: result);
      return right(result);
    } on ServerException catch (ex) {
      return left(ServerFailure(message: ex.message));
    } on CacheException catch (ex) {
      return left(CachedFailure(message: ex.message));
    }
  }

  @override
  Future<Either<Failure, bool>> requestOTP() async {
    try {
      final result = await authRemoteDataSource.requestOtp();
      return right(result);
    } on ServerException catch (ex) {
      return left(ServerFailure(message: ex.message));
    }
  }

  @override
  Future<Either<Failure, bool>> register(
      {required User user, required String password}) async {
    try {
      final result = await authRemoteDataSource.register(
          user: UserModel.fromEntity(user), password: password);
      return right(result);
    } on ServerException catch (ex) {
      return left(ServerFailure(message: ex.message));
    }
  }
}
