// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import 'package:get_skeleton/features/auth/domain/repositories/auth_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class LoginUseCase implements UseCase<bool, LoginParam> {
  final AuthRepository authRepository;

  LoginUseCase(this.authRepository);

  @override
  Future<Either<Failure, bool>> call(LoginParam params) {
    return authRepository.login(email: params.email, password: params.password);
  }
}

class LoginParam {
  final String email;
  final String password;

  LoginParam({required this.email, required this.password});

  @override
  String toString() => 'LoginParam(email: $email, password: $password)';

  @override
  bool operator ==(covariant LoginParam other) {
    if (identical(this, other)) return true;

    return other.email == email && other.password == password;
  }

  @override
  int get hashCode => email.hashCode ^ password.hashCode;
}
