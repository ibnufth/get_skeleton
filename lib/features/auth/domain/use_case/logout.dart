// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import 'package:get_skeleton/features/auth/domain/repositories/auth_repository.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class LogoutUseCase implements UseCase<bool, NoParams> {
  final AuthRepository authRepository;

  LogoutUseCase(this.authRepository);

  @override
  Future<Either<Failure, bool>> call(NoParams params) {
    return authRepository.logout();
  }
}
