// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/auth/domain/repositories/auth_repository.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class RegisterUseCase extends UseCase<bool, RegisterParams> {
  final AuthRepository authRepository;
  RegisterUseCase(
    this.authRepository,
  );

  @override
  Future<Either<Failure, bool>> call(RegisterParams params) {
    return authRepository.register(
        user: params.user, password: params.password);
  }
}

class RegisterParams {
  final User user;
  final String password;

  RegisterParams({required this.user, required this.password});

  @override
  bool operator ==(covariant RegisterParams other) {
    if (identical(this, other)) return true;

    return other.user == user && other.password == password;
  }

  @override
  int get hashCode => user.hashCode ^ password.hashCode;

  @override
  String toString() => 'RegisterParams(user: $user, password: $password)';
}
