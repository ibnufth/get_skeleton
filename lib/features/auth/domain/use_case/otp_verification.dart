// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import 'package:get_skeleton/features/auth/domain/repositories/auth_repository.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';

class OTPVerificationUseCase extends UseCase<User, String> {
  AuthRepository authRepository;
  OTPVerificationUseCase(this.authRepository);

  @override
  Future<Either<Failure, User>> call(String params) {
    return authRepository.otpVerification(params);
  }
}
