import 'package:dartz/dartz.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';

abstract class AuthRepository {
  Future<Either<Failure, bool>> login(
      {required String email, required String password});

  Future<Either<Failure, bool>> logout();

  Future<Either<Failure, User>> otpVerification(String otp);

  Future<Either<Failure, bool>> requestOTP();

  Future<Either<Failure, bool>> register(
      {required User user, required String password});
}
