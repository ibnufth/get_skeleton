import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/presentation/controllers/login.controller.dart';
import 'package:get_skeleton/features/widget/form.dart';
import 'package:get_skeleton/infrastructure/localization/gen/app_localizations.dart';
import 'package:get_skeleton/infrastructure/navigation/routes.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});

  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Card(
              child: FormBuilder(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 32),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      FormBuilderTextField(
                        name: "email",
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.email(),
                          FormBuilderValidators.required(),
                        ]),
                        decoration: InputDecoration(
                          label: Text(
                            "${AppLocalizations.of(context)?.email.capitalizeFirst}",
                          ),
                        ),
                      ),
                      const SizedBox(height: 16),
                      FormPassword(
                        name: "password",
                        label:
                            "${AppLocalizations.of(context)?.password.capitalizeFirst}",
                      ),
                      const SizedBox(height: 32),
                      GetBuilder<LoginController>(
                        builder: (loginController) {
                          if (loginController.isLoading.value) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          return FilledButton(
                            onPressed: () {
                              if (_formKey.currentState?.saveAndValidate() ??
                                  false) {
                                var data = _formKey.currentState?.value;
                                loginController.login(
                                  email: data?["email"],
                                  password: data?["password"],
                                  onFailure: (message) {
                                    Get.snackbar("Error", message);
                                  },
                                  onSuccess: () {
                                    Get.offAllNamed(Routes.OTP);
                                  },
                                );
                              }
                            },
                            child: Text(
                              "${AppLocalizations.of(context)?.login.capitalizeFirst}",
                            ),
                          );
                        },
                      ),
                      const SizedBox(height: 32),
                      const Text("belum punya akun?"),
                      const SizedBox(height: 8),
                      OutlinedButton(
                          onPressed: () {
                            Get.toNamed(Routes.REGISTER);
                          },
                          child: const Text("Register"))
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
