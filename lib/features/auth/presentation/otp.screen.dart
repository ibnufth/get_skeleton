import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/presentation/controllers/otp.controller.dart';
import 'package:get_skeleton/infrastructure/localization/gen/app_localizations.dart';

import 'package:pinput/pinput.dart';

class OTPScreen extends StatefulWidget {
  const OTPScreen({super.key});

  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(body: GetBuilder<OTPController>(
      builder: (controller) {
        return SafeArea(
          child: SingleChildScrollView(
            child: SizedBox(
              width: screenSize.width,
              height: screenSize.height,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "${AppLocalizations.of(context)?.verification}",
                        style: theme.textTheme.headlineLarge
                            ?.copyWith(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(height: 16),
                      Text("${AppLocalizations.of(context)?.enterOtpCode}"),
                      const SizedBox(height: 16),
                      const Text(
                        "email@example.com",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(height: 32),
                      Pinput(
                        length: 4,
                        onCompleted: controller.verifyOTP,
                      ),
                      const SizedBox(height: 32),
                      Text("${AppLocalizations.of(context)?.didntReceiveCode}"),
                      TextButton(
                        onPressed: () {
                          controller.requestOTP();
                        },
                        child: Text(
                          "${AppLocalizations.of(context)?.resend}",
                          style: const TextStyle(
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (controller.isLoading.value)
                    Container(
                      color: Colors.black12,
                      width: screenSize.width,
                      height: screenSize.height,
                      child: const Center(child: CircularProgressIndicator()),
                    ),
                ],
              ),
            ),
          ),
        );
      },
    ));
  }
}
