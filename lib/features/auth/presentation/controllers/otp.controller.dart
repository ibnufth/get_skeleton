import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/domain/use_case/otp_verification.dart';
import 'package:get_skeleton/features/auth/domain/use_case/request_otp.dart';
import 'package:get_skeleton/features/user/presentation/controller/user.controller.dart';
import 'package:get_skeleton/features/widget/snackbar.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';
import 'package:get_skeleton/infrastructure/navigation/routes.dart';

class OTPController extends GetxController {
  final OTPVerificationUseCase otpVerificationUseCase;
  final RequestOTPUseCase requestOTPUseCase;

  OTPController({
    required this.otpVerificationUseCase,
    required this.requestOTPUseCase,
  });

  RxBool isLoading = false.obs;

  verifyOTP(String otp) async {
    isLoading.value = true;
    update();

    final result = await otpVerificationUseCase(otp);
    result.fold((l) {
      errorSnackbar(
        "Gagal Verifikasi OTP",
        "${l.message}",
      );
    }, (r) {
      Get.find<UserController>().setActiveUser(r);
      Get.offAllNamed(Routes.HOME);
    });
    isLoading.value = false;
    update();
  }

  requestOTP() async {
    isLoading.value = true;
    update();

    final result = await requestOTPUseCase(NoParams());
    await Future.delayed(const Duration(seconds: 1));
    result.fold((l) {
      errorSnackbar(
        "Gagal meminta OTP",
        "${l.message}",
      );
    }, (r) {
      successSnackbar(
        "Sukses meminta OTP",
        "OTP telah dikirim ke email anda",
      );
    });
    isLoading.value = false;
    update();
  }
}
