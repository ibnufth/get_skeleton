import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/domain/use_case/register.dart';
import 'package:get_skeleton/features/user/domain/entities/user.dart';
import 'package:get_skeleton/features/widget/snackbar.dart';
import 'package:get_skeleton/infrastructure/navigation/routes.dart';

class RegisterController extends GetxController {
  final RegisterUseCase registerUseCase;

  RegisterController(this.registerUseCase);
  RxBool isLoading = false.obs;

  register(User user, String password) async {
    isLoading.value = true;
    update();

    final result =
        await registerUseCase(RegisterParams(user: user, password: password));
    result.fold((l) {
      errorSnackbar("Gagal Registrasi", "${l.message}");
    }, (r) {
      Get.toNamed(Routes.OTP);
    });
    isLoading.value = false;
    update();
  }
}
