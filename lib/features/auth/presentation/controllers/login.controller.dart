import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/domain/use_case/login.dart';

class LoginController extends GetxController {
  final LoginUseCase loginUseCase;

  LoginController(this.loginUseCase);

  RxBool isLoading = false.obs;

  login({
    required String email,
    required String password,
    Function()? onSuccess,
    Function(String message)? onFailure,
  }) async {
    isLoading.value = true;
    update();

    final result =
        await loginUseCase(LoginParam(email: email, password: password));
    result.fold((l) {
      onFailure?.call("${l.message}");
    }, (r) {
      onSuccess?.call();
    });
    isLoading.value = false;
    update();
  }
}
