import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:get_skeleton/features/auth/data/data_sources/auth_remote_data_source.dart';
import 'package:get_skeleton/features/auth/data/repositories/auth_repository_impl.dart';
import 'package:get_skeleton/features/auth/domain/repositories/auth_repository.dart';
import 'package:get_skeleton/features/auth/domain/use_case/logout.dart';
import 'package:get_skeleton/features/quote/data/models/quote_model.dart';
import 'package:get_skeleton/features/user/data/data_sources/user_local_data_source.dart';
import 'package:get_skeleton/features/user/data/data_sources/user_remote_data_source.dart';
import 'package:get_skeleton/features/user/data/repositories/user_repositories_impl.dart';
import 'package:get_skeleton/features/user/domain/repositories/user_repository.dart';
import 'package:get_skeleton/features/user/domain/use_case/get_active_user.dart';
import 'package:get_skeleton/features/user/presentation/controller/user.controller.dart';
import 'package:get_skeleton/infrastructure/services/api/api.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';

Future<void> setupDependencies() async {
  Get.put(const FlutterSecureStorage(), permanent: true);
  await Get.putAsync<Isar>(() async {
    final dir = await getApplicationDocumentsDirectory();
    return await Isar.open([
      QuoteModelSchema,
    ], directory: dir.path);
  }, permanent: true);
  Get.put(InternetConnection());
  Get.put(Api(apiClient: APIClient()), permanent: true);
  Get.put<UserLocalDataSource>(UserLocalDataSourceImpl(Get.find()),
      permanent: true);
  Get.put<UserRemoteDataSource>(UserRemoteDataSourceImpl(Get.find()),
      permanent: true);
  Get.put<UserRepository>(
      UserRepositoryImpl(
          userRemoteDataSource: Get.find(), userLocalDataSource: Get.find()),
      permanent: true);
  Get.lazyPut(() => GetActiveUser(Get.find()), fenix: true);
  Get.lazyPut<AuthRemoteDataSource>(
      () => AuthRemoteDataSourceImpl(Get.find(), Get.find()),
      fenix: true);
  Get.lazyPut<AuthRepository>(
      () => AuthRepositoryImpl(
          userLocalDataSource: Get.find(), authRemoteDataSource: Get.find()),
      fenix: true);
  Get.lazyPut(() => LogoutUseCase(Get.find()), fenix: true);
  Get.lazyPut(() => UserController(Get.find(), Get.find()), fenix: true);
}
