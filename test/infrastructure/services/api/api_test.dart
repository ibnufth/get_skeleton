import 'package:flutter_test/flutter_test.dart';
import 'package:get_skeleton/infrastructure/services/api/api.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'api_test.mocks.dart';

@GenerateMocks([APIClient])
void main() {
  group('API', () {
    late MockAPIClient mockAPIClient;
    late Api api;

    setUp(() {
      mockAPIClient = MockAPIClient();
      api = Api(apiClient: mockAPIClient);
    });

    test('should return an response when get called', () async {
      // arrange

      // act

      // assert
    });
  });
}
