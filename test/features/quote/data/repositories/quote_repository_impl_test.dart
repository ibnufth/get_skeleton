import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_local_data_source.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_remote_data_source.dart';
import 'package:get_skeleton/features/quote/data/repositories/quote_repository_impl.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../utils/fixtures/quote/quote.dart';
import '../../../../utils/helper/list_expect.dart';
// import '../../../../utils/helper/online_offline.dart';
import 'quote_repository_impl_test.mocks.dart';

@GenerateMocks([
  QuoteLocalDataSource,
  QuoteRemoteDataSource,
  InternetConnection,
])
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  late MockQuoteLocalDataSource mockQuoteLocalDataSource;
  late MockQuoteRemoteDataSource mockQuoteRemoteDataSource;
  late MockInternetConnection mockInternetConnection;
  late QuoteRepositoryImpl quoteRepositoryImpl;

  setUp(() {
    mockQuoteLocalDataSource = MockQuoteLocalDataSource();
    mockQuoteRemoteDataSource = MockQuoteRemoteDataSource();
    mockInternetConnection = MockInternetConnection();
    quoteRepositoryImpl = QuoteRepositoryImpl(
      quoteRemoteDataSource: mockQuoteRemoteDataSource,
      quoteLocalDataSource: mockQuoteLocalDataSource,
      internetConnection: mockInternetConnection,
    );
  });

  void runTestOnline(dynamic Function() body) {
    group('[Online]', () {
      setUp(() {
        when(mockInternetConnection.hasInternetAccess)
            .thenAnswer((realInvocation) async => true);
      });
      body();
    });
  }

  void runTestOffline(dynamic Function() body) {
    group('[Offline]', () {
      setUp(() {
        when(mockInternetConnection.hasInternetAccess)
            .thenAnswer((realInvocation) async => false);
      });
      body();
    });
  }

  group('getRandomQuotes', () {
    runTestOnline(() {
      test(
          'should return a random quote from remote data source when has internet connection',
          () async {
        // arrange
        when(mockQuoteRemoteDataSource.getRandomQuotes())
            .thenAnswer((realInvocation) async => [quoteModel]);
        when(mockQuoteLocalDataSource.saveQuotes(quotes: anyNamed("quotes")))
            .thenAnswer((realInvocation) async => true);
        // act
        final result = await quoteRepositoryImpl.getRandomQuotes();
        // assert
        expectListEither(result, [quoteModel]);
      });

      test(
          'should return local data when failed get data from remote data source',
          () async {
        // arrange
        when(mockQuoteRemoteDataSource.getRandomQuotes())
            .thenThrow(ServerException());
        when(mockQuoteLocalDataSource.getRandomQuotes())
            .thenAnswer((realInvocation) async => [quoteModel]);
        // act
        final result = await quoteRepositoryImpl.getRandomQuotes();
        // assert
        expectListEither(result, [quoteModel]);
      });
      test(
          'should return cache failure when failed save data to local data source',
          () async {
        // arrange
        when(mockQuoteRemoteDataSource.getRandomQuotes())
            .thenAnswer((realInvocation) async => [quoteModel]);
        when(mockQuoteLocalDataSource.saveQuotes(quotes: anyNamed("quotes")))
            .thenThrow(CacheException());
        // act
        final result = await quoteRepositoryImpl.getRandomQuotes();
        // assert
        expect(result, left(const CachedFailure()));
      });
      test(
          'should return cache failure when failed get data from local data source',
          () async {
        // arrange
        when(mockQuoteRemoteDataSource.getRandomQuotes())
            .thenThrow(ServerException());
        when(mockQuoteLocalDataSource.saveQuotes(quotes: anyNamed("quotes")))
            .thenAnswer((realInvocation) async => true);
        when(mockQuoteLocalDataSource.getRandomQuotes())
            .thenThrow(CacheException());
        // act
        final result = await quoteRepositoryImpl.getRandomQuotes();
        // assert
        expect(result, left(const CachedFailure()));
      });
    });
    runTestOffline(() {
      test('should return local when has no internet connection', () async {
        // arrange
        when(mockQuoteLocalDataSource.getRandomQuotes())
            .thenAnswer((realInvocation) async => [quoteModel]);
        // act
        final result = await quoteRepositoryImpl.getRandomQuotes();
        // assert
        expectListEither(result, [quoteModel]);
      });
      test('should return cache failure when failed get data from local',
          () async {
        // arrange
        when(mockQuoteLocalDataSource.getRandomQuotes())
            .thenThrow(CacheException());
        // act
        final result = await quoteRepositoryImpl.getRandomQuotes();
        // assert
        expect(result, left(const CachedFailure()));
      });
    });
  });
}
