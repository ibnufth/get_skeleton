import 'package:flutter_test/flutter_test.dart';
import 'package:get_skeleton/features/quote/data/models/quote_model.dart';

import '../../../../utils/fixtures/quote/quote.dart';

void main() {
  group('quote model', () {
    test('should return valid model from entity', () async {
      // act
      final result = QuoteModel.fromEntity(quote);
      // assert
      expect(result, quoteModel);
    });
    test('should return valid model when copy with insert value', () async {
      // act
      final result = quoteModel.copyWith(
        id: 2,
        english: "_english",
        indo: "_indo",
        character: "_character",
        anime: "_anime",
      );
      // assert
      expect(
          result,
          const QuoteModel(
            id: 2,
            english: "_english",
            indo: "_indo",
            character: "_character",
            anime: "_anime",
          ));
    });
    test('should return valid model when copy with insert null value',
        () async {
      // act
      final result = quoteModel.copyWith();
      // assert
      expect(result, quoteModel);
    });
    test('should return valid model from map', () async {
      // act
      final result = QuoteModel.fromMap(quoteMap);
      // assert
      expect(result, quoteModel);
    });
    test('should return valid map from model', () async {
      // act
      final result = quoteModel.toMap();
      // assert
      expect(result, quoteMap);
    });
  });
}
