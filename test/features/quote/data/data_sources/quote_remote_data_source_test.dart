import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_remote_data_source.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:get_skeleton/infrastructure/services/api/api.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../utils/fixtures/quote/quote.dart';
import '../../../../utils/helper/list_expect.dart';
import 'quote_remote_data_source_test.mocks.dart';

@GenerateMocks([Api])
void main() {
  late MockApi mockApi;
  late QuoteRemoteDataSourceImpl quoteRemoteDataSourceImpl;

  setUp(() {
    mockApi = MockApi();
    quoteRemoteDataSourceImpl = QuoteRemoteDataSourceImpl(api: mockApi);
  });

  Response response = Response(
    requestOptions: RequestOptions(),
    data: {
      "result": [
        quoteMap,
      ]
    },
    statusCode: 200,
  );

  group('getRandomQuotes', () {
    test('should return a random quote from api success', () async {
      // arrange
      when(mockApi.get(any)).thenAnswer((realInvocation) async => response);
      // act
      final result = await quoteRemoteDataSourceImpl.getRandomQuotes();
      // assert
      expectList(result, [quoteModel]);
    });
    test('should throw server exception when get quote from api failed',
        () async {
      // arrange
      when(mockApi.get(any)).thenThrow(DioException(
        requestOptions: RequestOptions(),
      ));
      // act
      final call = quoteRemoteDataSourceImpl.getRandomQuotes;
      // assert
      expect(call(), throwsA(const TypeMatcher<ServerException>()));
    });
  });
}
