import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:get_skeleton/features/quote/data/data_sources/quote_local_data_source.dart';
import 'package:get_skeleton/features/quote/data/models/quote_model.dart';
import 'package:get_skeleton/infrastructure/core/error/exception.dart';
import 'package:isar/isar.dart';
import 'package:path/path.dart' as path;

import '../../../../utils/fixtures/quote/quote.dart';

const bool kIsWeb = identical(0, 0.0);
void main() {
  late Isar isar;
  late QuoteLocalDataSourceImpl quoteLocalDataSourceImpl;

  setUp(() async {
    await Isar.initializeIsarCore(download: true);
    final dartToolDir = path.join(Directory.current.path, '.dart_tool');
    await Directory(dartToolDir).create(recursive: true);

    isar = await Isar.open(
      [QuoteModelSchema],
      directory: dartToolDir,
    );
    await isar.writeTxn(() async {
      await isar.quoteModels.clear();
    });
    quoteLocalDataSourceImpl = QuoteLocalDataSourceImpl(isar: isar);
  });

  tearDown(() async {
    if (isar.isOpen) {
      await isar.close();
    }
  });

  group('getRandomQuotes', () {
    test('should return data when get quote model', () async {
      // arrange
      await isar.writeTxn(() async {
        await isar.quoteModels.put(quoteModel);
      });
      // act
      final result = await quoteLocalDataSourceImpl.getRandomQuotes();
      // assert
      expect(result, [quote]);
    });

    test('should throw cache exception when get quote failed', () async {
      // arrange
      await isar.close();
      // act
      final call = quoteLocalDataSourceImpl.getRandomQuotes;
      // assert
      expect(call(), throwsA(const TypeMatcher<CacheException>()));
    });
  });

  group('saveQuotes', () {
    test('should return true when save quote model success', () async {
      // act
      final result =
          await quoteLocalDataSourceImpl.saveQuotes(quotes: [quoteModel]);
      // assert
      expect(result, true);
    });

    test('should throw cache exception when save quote failed', () async {
      // arrange
      await isar.close();
      // actv
      final call = quoteLocalDataSourceImpl.saveQuotes;
      // assert
      expect(call(quotes: [quoteModel]),
          throwsA(const TypeMatcher<CacheException>()));
    });
  });
}
