import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:get_skeleton/features/quote/domain/use_case/get_random_quote.dart';
import 'package:get_skeleton/features/quote/presentation/controllers/quote.controller.dart';
import 'package:get_skeleton/infrastructure/core/error/failures.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../utils/fixtures/quote/quote.dart';
import 'quote.controller_test.mocks.dart';

@GenerateMocks([GetRandomQuotes])
void main() {
  late MockGetRandomQuotes mockGetRandomQuotes;
  late QuoteController quoteController;

  setUp(() {
    Get.testMode = true;
    mockGetRandomQuotes = MockGetRandomQuotes();
    quoteController = QuoteController(getRandomQuotes: mockGetRandomQuotes);
  });

  group('getRandomQuotes', () {
    test('should expect isLoading [true, false] when get RandomQuote called',
        () async {
      // arrange
      when(mockGetRandomQuotes(NoParams()))
          .thenAnswer((realInvocation) async => right([quote]));
      // assert later
      expectLater(
          quoteController.isLoading.stream, emitsInOrder([true, false]));
      // act
      quoteController.getRandomQuotes();
    });
    test('should expect quotes [[], [quote]] when get RandomQuote called',
        () async {
      // arrange
      when(mockGetRandomQuotes(NoParams()))
          .thenAnswer((realInvocation) async => right([quote]));
      // assert later
      expectLater(
          quoteController.quotes.stream,
          emitsInOrder([
            [quote]
          ]));
      // act
      quoteController.getRandomQuotes();
    });
    test(
        'should expect error message ["CacheFailure"] when get RandomQuote called error',
        () async {
      // arrange
      when(mockGetRandomQuotes(NoParams())).thenAnswer((realInvocation) async =>
          left(const CachedFailure(message: "CacheFailure")));
      // assert later
      expectLater(
          quoteController.errorMessage.stream, emitsInOrder(["CacheFailure"]));
      // act
      quoteController.getRandomQuotes();
    });
  });
}
