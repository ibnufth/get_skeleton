import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_skeleton/features/quote/domain/repositories/quote_repository.dart';
import 'package:get_skeleton/features/quote/domain/use_case/get_random_quote.dart';
import 'package:get_skeleton/infrastructure/core/use_case/use_case.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../utils/fixtures/quote/quote.dart';
import '../../../../utils/helper/list_expect.dart';
import 'get_random_quote_test.mocks.dart';

@GenerateMocks([QuoteRepository])
void main() {
  late MockQuoteRepository mockQuoteRepository;
  late GetRandomQuotes getRandomQuotes;

  setUp(() {
    mockQuoteRepository = MockQuoteRepository();
    getRandomQuotes = GetRandomQuotes(mockQuoteRepository);
  });

  test('should return data when call get radom quotes successfully', () async {
    // arrange
    when(mockQuoteRepository.getRandomQuotes())
        .thenAnswer((realInvocation) async => right([quote]));
    // act
    final result = await getRandomQuotes(NoParams());
    // assert
    expectListEither(result, [quote]);
  });
}
