import 'package:flutter_test/flutter_test.dart';
import 'package:get_skeleton/features/quote/domain/entites/quote.dart';

import '../../../../utils/fixtures/quote/quote.dart';

void main() {
  group('Quote Entity', () {
    test('should return valid entities when copy with insert values', () async {
      // act
      final result = quote.copyWith(
        id: 2,
        english: "_english",
        indo: "_indo",
        character: "_character",
        anime: "_anime",
      );
      // assert
      expect(
          result,
          const Quote(
            id: 2,
            english: "_english",
            indo: "_indo",
            character: "_character",
            anime: "_anime",
          ));
    });
    test('should return valid entities when copy with insert null values',
        () async {
      // act
      final result = quote.copyWith();
      // assert
      expect(result, quote);
    });
    test('should return valid string when toString is called', () async {
      // arrange

      // act
      final result = quote.toString();
      // assert
      expect(result, isA<String>());
    });
  });
}
