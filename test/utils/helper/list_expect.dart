import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:logger/logger.dart';

expectList(List actual, List expected) {
  var isEquals = listEquals(actual, expected);
  if (!isEquals) {
    Logger log = Logger();
    log.e("List Not Equals\nExpected $expected\n\tActual$actual");
  }
  expect(isEquals, true);
}

expectListEither(Either actual, List expected) {
  actual.fold((l) {
    expect(l, expected);
  }, (r) {
    var isEquals = listEquals(r, expected);
    if (!isEquals) {
      Logger log = Logger();
      log.e("List Not Equals\nExpected $expected\n\tActual$actual");
    }
    expect(isEquals, true);
  });
}
