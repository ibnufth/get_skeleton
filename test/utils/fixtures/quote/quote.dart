import 'package:get_skeleton/features/quote/data/models/quote_model.dart';
import 'package:get_skeleton/features/quote/domain/entites/quote.dart';

var quote = const Quote(
  id: 1,
  english: "one",
  indo: "satu",
  character: "saya",
  anime: "kamu",
);

var quoteModel = const QuoteModel(
  id: 1,
  english: "one",
  indo: "satu",
  character: "saya",
  anime: "kamu",
);

var quoteMap = {
  "id": 1,
  "english": "one",
  "indo": "satu",
  "character": "saya",
  "anime": "kamu"
};
